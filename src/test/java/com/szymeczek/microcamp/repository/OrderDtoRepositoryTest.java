package com.szymeczek.microcamp.repository;

import com.szymeczek.microcamp.MicrocampApplication;
import com.szymeczek.microcamp.domain.Customer;
import com.szymeczek.microcamp.domain.Order;
import com.szymeczek.microcamp.domain.Order_;
import com.szymeczek.microcamp.services.OrderService;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = MicrocampApplication.class)
@TestPropertySource(locations="classpath:application.properties")
 class OrderDtoRepositoryTest {
    @Autowired
    OrderRepository orderRepository;

    @Autowired

    CustomerRepository customerRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    OrderService orderService;




    @Test
    @Transactional
    void testReadOrderWithCustomer() {
        Customer customer = customerRepository.save(new Customer(null, "Krzysztof", "Kowalski"));
        orderRepository.save(new Order(null, BigDecimal.ONE, customer));
        orderRepository.save(new Order(null, BigDecimal.TEN, customerRepository.save(new Customer(null, "Krzysztof", "Kowalski"))));

        entityManager.flush();
        entityManager.clear();
//        Optional<Order> orderById = orderRepository.findById(1L);
        orderRepository.findAll().forEach(a -> a.getCustomer().getFirstName());

//        System.out.println("orderById = " + orderById.get().getCustomer());

    }

    @Test
    void testFindByPremiumUserPrice() {
        Customer customer = customerRepository.save(new Customer(null, "Krzysztof", "Kowalski"));
        orderRepository.save(new Order(null, BigDecimal.ONE, customer));
        orderRepository.save(new Order(null, BigDecimal.TEN, customer));
        orderService.findPremiumOrder();
    }
}
