package com.szymeczek.microcamp.repository;

import com.szymeczek.microcamp.domain.Customer;
import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@DataJpaTest
@ExtendWith(SpringExtension.class)
class CustomerRepositoryTest {

    public static final Customer CUSTOMER_1 = new Customer(null, "Krzysztof", "Kowalski");
    public static final Customer CUSTOMER_2 = new Customer(null, "Ziutek", "Ziutkowski");
    @Autowired
    CustomerRepository customerRepository;


    @Test
    void testFindByFistName() {
//GIVEN
        Customer saved = customerRepository.save(CUSTOMER_1);
        Customer saved2 = customerRepository.save(CUSTOMER_2);
        String fistName = "Krzysztof";

//WHEN
        List<Customer> customers = customerRepository.findByFirstName(fistName);

        Assertions.assertThat(customers).hasSize(1);
        Assertions.assertThat(customers)
                .extracting(Customer::getFirstName, Customer::getLastName)
                .contains(
                        Tuple.tuple(fistName, CUSTOMER_1.getLastName())
                );
    }
}