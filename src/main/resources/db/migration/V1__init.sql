create table customer
(
    id         int          not null
        primary key,
    first_name varchar(255) not null,
    last_name  varchar(255) null
);

create table customer_seq
(
    next_val bigint null
);

create table order_customer
(
    customer_id int            null,
    price       decimal(38, 2) not null,
    id          bigint         not null
        primary key,
    constraint order_customer_FK
        foreign key (customer_id) references customer (id)
);

create table order_customer_seq
(
    next_val bigint null
);

