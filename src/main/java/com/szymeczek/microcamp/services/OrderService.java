package com.szymeczek.microcamp.services;

import com.szymeczek.microcamp.config.ApplicationProperties;
import com.szymeczek.microcamp.domain.Customer;
import com.szymeczek.microcamp.domain.Customer_;
import com.szymeczek.microcamp.domain.Order;
import com.szymeczek.microcamp.domain.Order_;
import com.szymeczek.microcamp.dto.OrderDto;
import com.szymeczek.microcamp.repository.OrderRepository;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    private final ApplicationProperties applicationProperties;

    private static Specification<Order> onlyPremiumPrice() {
        return (root, query, criteriaBuilder) -> {
//            criteriaBuilder.and(criteriaBuilder.greaterThan(root.get(Order_.price), new BigDecimal(100)));

            return criteriaBuilder.greaterThan(root.get(Order_.price), new BigDecimal(100));
        };
    }

    private static Specification<Order> customerName(String firstName) {
        return (root, query, criteriaBuilder) ->
        {
            Join<Order, Customer> joinCustomer = root.join(Order_.customer, JoinType.LEFT);
            return criteriaBuilder.and(criteriaBuilder.equal(joinCustomer.get(Customer_.firstName), firstName));
        };
    }

    private static Specification<Order> onlySpecialUser(Customer customer) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Order_.customer), customer);
    }

    public List<OrderDto> findPremiumOrder() {
        Specification<Order> filterSpecification = onlyPremiumPrice();
        if (applicationProperties.isDevMode()) {
            filterSpecification = filterSpecification.or(onlySpecialUser(new Customer(1)));
        }
        filterSpecification = filterSpecification.and(customerName("Krzysztof"));
//        filterSpecification = Specification.anyOf(onlyPremiumPrice(), onlySpecialUser(new Customer(1))).and(onlyPremiumPrice());

//        orderRepository.findAll(new OnlyPremiumSpecification(BigDecimal.TEN));


      /*  if (applicationProperties.isDevMode()) {
            orderRepository.findByPriceGreaterThanOrCustomer(BigDecimal.TEN, new Customer(1));
        } else {
            orderRepository.findByPriceGreaterThan(BigDecimal.TEN);
        }*/
//        orderRepository.findAll(new OnlyPremiumSpecification(BigDecimal.TEN));
        return orderRepository.findAll(filterSpecification)
                .stream().map(this::mapDto)
                .toList();
    }

    public OrderDto update(OrderDto orderDto) {
        if (!orderRepository.existsById(orderDto.getId())) {
            throw new RuntimeException();
        }
        Order savedOrder = orderRepository.save(mapOrder(orderDto));
        return mapDto(savedOrder);
    }

    private OrderDto mapDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setPrice(order.getPrice());
        orderDto.setCreatedBy(order.getCreatedBy());
        orderDto.setCreatedDate(order.getCreatedDate());
        orderDto.setCustomerId(order.getCustomer().getId());
        orderDto.setModifiedBy(order.getModifiedBy());
        orderDto.setLastModifyDate(order.getLastModifyDate());
        orderDto.setVersion(order.getVersion());

//....
        return orderDto;
    }

    private Order mapOrder(OrderDto orderDto) {
        Order order = new Order(orderDto.getId(), orderDto.getPrice(), new Customer(orderDto.getCustomerId()));
        order.setVersion(orderDto.getVersion());
        return order;
    }

    public List<OrderDto> findAll() {
        return orderRepository.findAll().stream().map(this::mapDto).toList();
    }

    private static class OnlyPremiumSpecification implements Specification<Order> {

        private BigDecimal price;

        private OnlyPremiumSpecification(BigDecimal price) {
            this.price = price;
        }

        @Override
        public Predicate toPredicate(Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.greaterThanOrEqualTo(root.get(Order_.price), price);
        }
    }
}
