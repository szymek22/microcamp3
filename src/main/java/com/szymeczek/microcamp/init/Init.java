package com.szymeczek.microcamp.init;

import com.szymeczek.microcamp.domain.Customer;
import jakarta.annotation.PostConstruct;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Init {
    private final JdbcTemplate jdbcTemplate;

    public Init(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostConstruct
    public void init() {
      /*  jdbcTemplate.execute("CREATE TABLE CUSTOMER(id Integer not null, first_name VARCHAR(255), last_name VARCHAR(255), constraint PK_CUSTOMER primary key (id))");
        Customer customer = new Customer(1, "Krzyszof", "Kowalski");
        Customer customer2 = new Customer(2, "Ziutek", "Ziutkowski");

        List<Object[]> input = List.of(customer, customer2)
                .stream().map(c->List.of(String.valueOf(c.getI()), c.getFistName(), c.getLastName())
                .toArray()).toList();

        jdbcTemplate.batchUpdate("INSERT INTO CUSTOMER(id, first_name, last_name) values (?, ?, ? )", input);

        List<Customer> customers = jdbcTemplate.query("SELECT id, first_name, last_name from customer where id = ?", (rs, rowNum) ->
                        new Customer(
                                rs.getInt("id"),
                                rs.getString("first_name"),
                                rs.getString("last_name")
                        )
                , 1
        );
        System.out.println("customers = " + customers);*/

    }
}
