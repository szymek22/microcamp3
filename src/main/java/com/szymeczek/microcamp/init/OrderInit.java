package com.szymeczek.microcamp.init;

import com.szymeczek.microcamp.domain.Customer;
import com.szymeczek.microcamp.domain.Order;
import com.szymeczek.microcamp.repository.CustomerRepository;
import com.szymeczek.microcamp.repository.OrderRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class OrderInit {

    private final OrderRepository orderRepository;

    private final
    CustomerRepository customerRepository;

    @PostConstruct
    public void init() {
        Customer customer = customerRepository.save(new Customer(null, "Krzysztof", "Kowalski"));
        orderRepository.save(new Order(null, BigDecimal.ONE, customer));
        orderRepository.save(new Order(null, BigDecimal.TEN, customerRepository.save(new Customer(null, "Krzysztof", "Kowalski"))));
    }
}
