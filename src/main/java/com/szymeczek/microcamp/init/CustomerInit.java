package com.szymeczek.microcamp.init;

import com.szymeczek.microcamp.domain.Customer;
import com.szymeczek.microcamp.repository.CustomerRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CustomerInit {
    private final CustomerRepository customerRepository;

    @PostConstruct
    public void init() {
        Customer customer = new Customer(null, "Krzysztof", "Kowalski");
        Customer saved = customerRepository.save(customer);
        customerRepository.findById(saved.getId());
        List<Customer> customers = customerRepository.findByFirstName("Krzysztof");
        Page<Customer> customersPaged = customerRepository.findByLastName("Kowalski", PageRequest.of(0, 10));
        System.out.println("customers = " + customers);
        System.out.println("customers = " + customersPaged.getTotalElements() + " " + customersPaged.getContent());
    }
}
