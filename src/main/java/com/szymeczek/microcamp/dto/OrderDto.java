package com.szymeczek.microcamp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    private Long id;

    private BigDecimal price;
    private Integer customerId;
    private Integer version;

    private Instant lastModifyDate;
    private Instant createdDate;

    private String createdBy;
    private String modifiedBy;

}
