package com.szymeczek.microcamp.repository;

import com.szymeczek.microcamp.domain.Customer;
import com.szymeczek.microcamp.domain.Order;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {
    @Query(value = "SELECT o from Order o left join fetch o.customer")
//    @EntityGraph(attributePaths = "customer")
    List<Order> findAllWithCustomer();
    List<Order> findByPriceGreaterThanOrCustomer(BigDecimal price, Customer customer);
    List<Order> findByPriceGreaterThan(BigDecimal price);
}
