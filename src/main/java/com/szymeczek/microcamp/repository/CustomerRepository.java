package com.szymeczek.microcamp.repository;

import com.szymeczek.microcamp.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Page<Customer> findByLastName(String lastName, Pageable pageable);

    List<Customer> findByFirstName(String fistName);
}

