package com.szymeczek.microcamp;

import com.szymeczek.microcamp.dto.OrderDto;
import com.szymeczek.microcamp.repository.OrderRepository;
import com.szymeczek.microcamp.services.OrderService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderRepository orderRepository;

    private final OrderService orderService;

    @GetMapping("")
    @Transactional
    public List<OrderDto> listOrders() {
        return orderService.findAll();
    }

    @PostMapping(value = "/{id}")
    public ResponseEntity<OrderDto> update(@PathVariable Long id, @RequestBody OrderDto order) {
        order.setId(id);
        return ResponseEntity.ok(orderService.update(order));
    }
}
